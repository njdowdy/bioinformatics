import asyncio
import json
from dataclasses import dataclass
from pathlib import Path

from bioinformatics.functions.blast import (
    create_blast_db,
    remove_gaps,
    single_fasta_blast,
)
from bioinformatics.functions.consensus import mergeConsensusFiles, mergeFastaFiles
from bioinformatics.functions.parse_blast import (
    multiprocess_extract_loci,
    multiprocess_fasta_operations,
    multiprocess_genome,
    parse_blast_hits,
    parse_reciprocal_blast_hits,
)
from bioinformatics.functions.tasks import (
    async_generate_consensus,
    async_pull_most_similar_taxon,
    generate_extraction_id,
)

# set up constants
algorithm = "blastn"
consensus_threshold = 50
input_fasta_location = "fasta/NOC1"
# set up paths
input_folder = Path("bioinformatics/input")
output_folder = Path("bioinformatics/output")
blastdb_location = output_folder / "blastdb"
blast_hits_location = output_folder / "blast_hits"
parsed_blast_results_location = output_folder / "parsed_blast_results"
query_folder = (
    input_folder / input_fasta_location
)  # you could also for each fasta file in this folder
consensus_seqs_location = output_folder / f"consensus_seqs/{input_fasta_location}"
ref_folder = input_folder / "reference_seqs"
ref_folder_bottom = ref_folder.name
similar_taxon_file = ref_folder / "similar_taxon_sets.json"
# ensure all output folders exist
output_folder.mkdir(parents=True, exist_ok=True)
blastdb_location.mkdir(parents=True, exist_ok=True)
blast_hits_location.mkdir(parents=True, exist_ok=True)
parsed_blast_results_location.mkdir(parents=True, exist_ok=True)
consensus_seqs_location.mkdir(parents=True, exist_ok=True)


# define utility functions
def is_target_file(filename: str, unique_string: str) -> bool:
    """Check if a file is a target file

    Args:
        filename (str): The name of the file
        unique_string (str): The unique string to search for in the file name

    Returns:
        bool: True if the file is a target file, False otherwise
    """
    if unique_string in filename and filename.endswith(".fasta"):
        return True
    return False


def cleanup(unique_string: str) -> None:
    """Remove all files in the query folder that contain the unique string in their name

    Args:
        unique_string (str): The unique string to search for in the file name
    """
    # List all the files in the folder
    files = query_folder.iterdir()
    # Iterate through the list of files
    for file in files:
        if is_target_file(file.name, unique_string):
            file.unlink()


@dataclass
class SimilarTaxon:
    genome: str
    similar_taxa: list[str]


# initialize some variables
probekit_db = Path("")
first_loop = True
# process each genome in the reference folder
for file in ref_folder.iterdir():
    if not first_loop:
        query_folder = output_folder / f"seq_extractions/{input_fasta_location}"
    else:
        # extract the longest, most complete sequence from each fasta file in the query folder
        temp = multiprocess_extract_loci(query_folder)
        # remove gaps from the extracted sequences
        asyncio.run(remove_gaps(temp))
        # create a blast database from these sequences
        probekit_db = create_blast_db(temp)
        # delete temp folder
        temp.unlink()
    if file.suffix == ".fna":
        ref_file = file
        # ref_file = "genome_spilosoma_lubricepidum.fna"
        ref_file_no_ext = ref_file.stem
        # list of taxa to extract sequences from for blast
        # most preferred choice listed first
        # for more general groups (e.g., subtribe), the first sequence in the alignment matched will be used
        with open(similar_taxon_file, "r") as file:
            data_dict = json.load(file)
            # Deserialize the JSON data into a list of SimilarTaxon objects
            data = [SimilarTaxon(**item) for item in data_dict]
        similar_taxa = None
        # Search for the entry with the matching "genome" key value
        for entry in data:
            if (
                entry.genome.replace(" ", "_").lower()
                in ref_file_no_ext.replace(" ", "_").lower()
            ):
                similar_taxa = [x.replace(" ", "_") for x in entry.similar_taxa]
                break
        if similar_taxa is not None:
            # generate extraction id
            extraction_id = generate_extraction_id(
                f"{ref_file_no_ext}{''.join(similar_taxa)}"
            )
            # extract sequences of similar taxa from all fasta files
            asyncio.run(
                async_pull_most_similar_taxon(query_folder, similar_taxa, extraction_id)
            )
            # merge all files with grouping in name
            asyncio.run(mergeFastaFiles(query_folder, extraction_id))
            # remove any gaps from the extracted sequences
            asyncio.run(remove_gaps(query_folder / f"merged_{extraction_id}.fasta"))
            #### PERFORM BLAST ####
            # create BLAST database from genome
            genome_db = create_blast_db(ref_file)
            # use blastn: balanced BLAST algorithm
            blast_result_file = (
                blast_hits_location
                / algorithm
                / f"merged_{extraction_id}_gapless_VS_{ref_folder_bottom}__{ref_file_no_ext}"
            )
            single_fasta_blast(
                query_folder / f"merged_{extraction_id}_gapless.fasta",
                blastdb_location / f"{ref_folder_bottom}__{ref_file_no_ext}",
                blast_result_file,
                algorithm,
                # word_size=24,
                # mismatch_penalty=-1,
                # gapopen_penalty=2,
                # gapextend_penalty=1,
            )
            single_fasta_blast(
                Path("bioinformatics/output/temp/NOC1/extract_gapless.fasta"),
                Path("bioinformatics/output/blastdb/NOC1__extract"),
                Path("bioinformatics/output/blast_hits/blastn/self_blast"),
                algorithm,
            )
            # parse results of BLAST blastn on close sequences
            parse_blast_hits(
                blast_result_file,
                query_folder,
            )
            # use parsed results to extract sequences from reference in the correct orientation and inject into the proper query sequence fasta file
            instruction_file_location = (
                parsed_blast_results_location
                / blast_result_file.relative_to(blast_hits_location)
            )
            instruction_file = Path(str(instruction_file_location) + ".csv")
            #### PERFORM RECIPROCAL BLAST ####
            # write out extracted sequences to temporary location for reciprocal blast
            # ?: takes about 10 minutes
            multiprocess_genome(instruction_file, ref_folder, query_folder)
            # perform reciprocal blast
            reciprocal_blast_result_file = (
                blast_hits_location
                / algorithm
                / f"{ref_folder_bottom}__{ref_file_no_ext}_VS_merged_{extraction_id}_gapless"
            )
            genome_extract_path = Path(
                f"{str(query_folder).replace('input/fasta','output/temp')}/genome_extracts.fasta"
            )
            single_fasta_blast(
                genome_extract_path,
                probekit_db,
                reciprocal_blast_result_file,
                algorithm,
                # word_size=24,
                # mismatch_penalty=-1,
                # gapopen_penalty=2,
                # gapextend_penalty=1,
            )
            # parse results of BLAST blastn on close sequences
            parse_reciprocal_blast_hits(
                reciprocal_blast_result_file,  # reference_vs_query.csv
                instruction_file,  # query_vs_reference.csv
            )
            multiprocess_fasta_operations(
                instruction_file,
                ref_folder,
                query_folder,  #! genome_extract_path
            )
            #### COMPARE BLAST AND RECIPROCAL BLAST RESULTS ####
            # if results pass both checks, append extracted sequence to the appropriate fasta file
            multiprocess_fasta_operations(instruction_file, ref_folder, query_folder)
            #### PERFORM CLEANUP ####
            cleanup(extraction_id)
        else:
            # revert back to original aligned query folder
            # because sequences need to be aligned to calculate consensus
            query_folder = input_folder / input_fasta_location
            # extract consensus sequences of all taxa from all fasta files
            asyncio.run(
                async_generate_consensus(query_folder, consensus_seqs_location, 50)
            )
            # merge consensus sequences
            asyncio.run(mergeConsensusFiles(consensus_seqs_location))
            merged_file = f"merged_{consensus_threshold}perc_consensus"
            # remove any gaps from the consensus sequences
            asyncio.run(remove_gaps(consensus_seqs_location / f"{merged_file}.fasta"))
            merged_gapless_file = merged_file + "_gapless"
            gapless_merged_consensus_file = (
                consensus_seqs_location / f"{merged_gapless_file}.fasta"
            )
            # create BLAST database from genome
            create_blast_db(ref_file)
            # use blastn: balanced BLAST algorithm
            blast_result_file = (
                blast_hits_location
                / algorithm
                / f"{merged_gapless_file}_VS_{ref_folder_bottom}__{ref_file_no_ext}"
            )
            single_fasta_blast(
                gapless_merged_consensus_file,
                blastdb_location / f"{ref_folder_bottom}__{ref_file_no_ext}",
                blast_result_file,
                algorithm,
                # word_size=24,
                # mismatch_penalty=-1,
                # gapopen_penalty=2,
                # gapextend_penalty=1,
            )
            # parse results of BLAST blastn on close sequences
            parse_blast_hits(
                blast_result_file,
                query_folder,
            )
            # use parsed results to extract sequences from reference in the correct orientation and inject into the proper query sequence fasta file
            instruction_file_location = (
                parsed_blast_results_location
                / blast_result_file.relative_to(blast_hits_location)
            )
            instruction_file = Path(str(instruction_file_location) + ".csv")
            if not first_loop:
                query_folder = output_folder / f"seq_extractions/{input_fasta_location}"
            multiprocess_fasta_operations(instruction_file, ref_folder, query_folder)
            # cleanup files that are no longer needed
            cleanup(f"{consensus_threshold}perc_")
        first_loop = False

from pathlib import Path

from bioinformatics.functions.genome_loci_extractor import extract_loci_from_genomes
from bioinformatics.functions.self_blast import perform_self_blast

# define inputs
input_folder = Path("bioinformatics/input")
query_loci = input_folder / "fasta/NOC1"
genomes = Path("bioinformatics/input/reference_seqs")

# STEP 1: perform self-blast to ensure that each query locus is unique
perform_self_blast(query_loci)

# STEP 2: perform blast search of query loci against each genomic resource
extract_loci_from_genomes(query_loci, genomes)

# STEP 3: perform reciprocal blast search of each genome-extracted locus against query loci

# STEP 4: realign each locus if necessary (? or will it still be aligned), check ORFs (? or will it still be in ORF 1)

# STEP 5: trim each locus alignment

# NEXT STEPS SHOULD PROBABLY BE A DIFFERENT WORKFLOW

# STEP 6: create supermatrix and partition files (partitioning checks for respective distance along chromosome)

# STEP 7: create other supermatrix datasets (e.g., AA, degen, NT12, NT123, CODON, 50%/75%/90% taxon occupancy, 50%/75%/90% site occupancy, flanks vs probes, bio-TIGER binned sites, loci with length > 1000bp, etc.)

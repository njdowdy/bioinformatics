import asyncio
import hashlib
import time
from functools import wraps
from pathlib import Path
from typing import Callable, Optional

from tqdm import tqdm
from tqdm.asyncio import tqdm as async_tqdm

from bioinformatics.functions.consensus import (
    generate_consensus,
    generate_consensus_sync,
    pull_most_similar_taxon,
)


def ensure_directory_exists(dir_path: Path):
    if not dir_path.exists():
        dir_path.mkdir(parents=True, exist_ok=True)


def generate_extraction_id(seed_string: str, length: int = 10) -> str:
    """Generate extraction id from seed string.

    Args:
        seed_string (str): Seed string to generate extraction id from.
        length (int, optional): Length of extraction id. Defaults to 10.

    Returns:
        str: Extraction id.
    """
    extraction_id = hashlib.sha256(seed_string.encode("utf-8")).hexdigest()[0:length]
    return extraction_id


class FileIterator:
    def __init__(self, input_folder, progress_bar):
        self.file_queue = asyncio.Queue()
        input_folder_path = Path(input_folder)
        for file_path in input_folder_path.glob("**/*"):
            if file_path.is_file():
                self.file_queue.put_nowait(file_path)
        self.progress_bar = progress_bar

    def __aiter__(self):
        return self

    async def __anext__(self):
        if self.file_queue.empty():
            raise StopAsyncIteration
        file = await self.file_queue.get()
        self.progress_bar.update(1)
        return file


def processAsDirectory(func: Callable):
    """Decorator to process a directory of files.

    Args:
        func (Callable): Function to process files.
    """

    @wraps(func)
    async def wrapper(input_folder: Path, *args, **kwargs):
        """Wrapper function to process a directory of files.

        Args:
            input_folder (Path): Path to input folder.
        """
        ensure_directory_exists(input_folder)
        start_time = time.perf_counter()
        tasks = []
        file_queue = asyncio.Queue()
        for path in input_folder.iterdir():
            if path.is_file():
                file_queue.put_nowait(path)
        file_count = file_queue.qsize()
        progress_bar = async_tqdm(total=file_count, ncols=100, smoothing=0.05)

        async def wrapped_func(fasta, *args, **kwargs):
            await func(fasta, *args, **kwargs)
            progress_bar.update(1)

        async def worker():
            while not file_queue.empty():
                fasta = await file_queue.get()
                tasks.append(asyncio.create_task(wrapped_func(fasta, *args, **kwargs)))

        await worker()
        await asyncio.gather(*tasks)
        progress_bar.close()
        end_time = time.perf_counter()
        elapsed_time = end_time - start_time
        if len(tasks) == 0:
            print("No tasks to process")
        elif len(tasks) == 1:
            print(f"Total execution time for 1 tasks: {elapsed_time:.2f} seconds")
        else:
            print(
                f"Total execution time for {len(tasks)} tasks: {elapsed_time:.2f} seconds"
            )

    return wrapper


def processAsDirectory_withWorkers(func: Callable, max_workers: int = 5):
    """Decorator to process a directory of files with workers.

    Args:
        func (Callable): Function to process files.
        max_workers (int, optional): Number of workers to use. Defaults to 5.
    """

    @wraps(func)
    async def wrapper(input_folder: Path, *args, **kwargs):
        """Wrapper function to process a directory of files with workers.

        Args:
            input_folder (Path): Path to input folder.
        """
        ensure_directory_exists(input_folder)
        start_time = time.perf_counter()

        async def worker(iterator):
            async for fasta in iterator:
                await func(fasta, *args, **kwargs)

        file_queue = asyncio.Queue()

        for path in input_folder.iterdir():
            if path.is_file():
                file_queue.put_nowait(path)

        file_count = file_queue.qsize()

        progress_bar = async_tqdm(total=file_count, ncols=100, smoothing=0.05)

        file_iterator = FileIterator(input_folder, progress_bar)

        worker_tasks = [
            asyncio.create_task(worker(file_iterator)) for _ in range(max_workers)
        ]

        await asyncio.gather(*worker_tasks)

        progress_bar.close()

        end_time = time.perf_counter()
        elapsed_time = end_time - start_time
        if file_count == 0:
            print("No tasks to process")
        elif file_count == 1:
            print(f"Total execution time for 1 tasks: {elapsed_time:.2f} seconds")
        else:
            print(
                f"Total execution time for {file_count} tasks: {elapsed_time:.2f} seconds"
            )

    return wrapper


from concurrent.futures import ThreadPoolExecutor, as_completed


def processAsDirectory_withThreads(func: Callable, max_workers: int = 8):
    """Decorator to process a directory of files with threads.

    Args:
        func (Callable): Function to process files.
        max_workers (int, optional): Number of workers to use. Defaults to 8.
    """

    @wraps(func)
    async def wrapper(input_folder: Path, *args, **kwargs):
        """Wrapper function to process a directory of files with threads.

        Args:
            input_folder (Path): Path to input folder.
        """
        ensure_directory_exists(input_folder)
        start_time = time.perf_counter()

        filepaths = []
        for path in input_folder.iterdir():
            if path.is_file():
                filepaths.append(path)

            if path.is_dir():
                for subpath in path.iterdir():
                    if subpath.is_file():
                        filepaths.append(subpath)

        async def wrapped_func(fasta):
            await func(fasta, *args, **kwargs)

        progress_bar = async_tqdm(total=len(filepaths), ncols=100, smoothing=0.05)

        semaphore = asyncio.Semaphore(max_workers)

        async def worker(filepath):
            async with semaphore:
                await wrapped_func(filepath)
                progress_bar.update(1)

        tasks = [worker(filepath) for filepath in filepaths]
        await asyncio.gather(*tasks)

        progress_bar.close()

        end_time = time.perf_counter()
        elapsed_time = end_time - start_time
        # number of files in input folder
        file_count = len(filepaths)
        if file_count == 0:
            print("No tasks to process")
        elif file_count == 1:
            print(f"Total execution time for 1 tasks: {elapsed_time:.2f} seconds")
        else:
            print(
                f"Total execution time for {file_count} tasks: {elapsed_time:.2f} seconds"
            )

    return wrapper


def processAsDirectory_withThreadsIO(func: Callable, max_workers: int = 8):
    """Decorator to process a directory of files with threads.

    Args:
        func (Callable): Function to process files.
        max_workers (int, optional): Number of workers to use. Defaults to 8.
    """

    @wraps(func)
    def wrapper(input_folder: Path, *args, **kwargs):
        """Wrapper function to process a directory of files with threads.

        Args:
            input_folder (Path): Path to input folder.
        """
        ensure_directory_exists(input_folder)

        async def main():
            start_time = time.perf_counter()

            filepaths = []
            for path in input_folder.iterdir():
                if path.is_file():
                    filepaths.append(path)

                if path.is_dir():
                    for subpath in path.iterdir():
                        if subpath.is_file():
                            filepaths.append(subpath)

            with ThreadPoolExecutor(max_workers=max_workers) as executor:
                with tqdm(
                    total=len(filepaths), ncols=100, smoothing=0.05
                ) as progress_bar:
                    loop = asyncio.get_event_loop()
                    tasks = [
                        loop.run_in_executor(executor, func, fasta, *args, **kwargs)
                        for fasta in filepaths
                    ]

                    for task in asyncio.as_completed(tasks):
                        await task  # To raise any exceptions from the task
                        progress_bar.update(1)

            end_time = time.perf_counter()
            elapsed_time = end_time - start_time
            # number of files in input folder
            file_count = len(filepaths)
            if file_count == 0:
                print("No tasks to process")
            elif file_count == 1:
                print(f"Total execution time for 1 tasks: {elapsed_time:.2f} seconds")
            else:
                print(
                    f"Total execution time for {file_count} tasks: {elapsed_time:.2f} seconds"
                )

        asyncio.run(main())

    return wrapper


@processAsDirectory
async def async_generate_consensus(
    fasta: Path, output: Path, threshold: Optional[int] = 50
):
    """Generate consensus sequence from a fasta file.

    Args:
        fasta (Path): Path to fasta file.
        output (Path): Path to output file.
        threshold (Optional[int], optional): Threshold for consensus. Defaults to 50.
    """
    await generate_consensus(fasta, output, threshold)


@processAsDirectory_withWorkers
async def async_worker_generate_consensus(
    fasta: Path, output: Path, threshold: Optional[int] = 50
):
    """Generate consensus sequence from a fasta file.

    Args:
        fasta (Path): Path to fasta file.
        output (Path): Path to output file.
        threshold (Optional[int], optional): Threshold for consensus. Defaults to 50.
    """
    await generate_consensus(fasta, output, threshold)


@processAsDirectory_withThreads
async def async_threads_generate_consensus(
    fasta: Path, output: Path, threshold: Optional[int] = 50
):
    """Generate consensus sequence from a fasta file.

    Args:
        fasta (Path): Path to fasta file.
        output (Path): Path to output file.
        threshold (Optional[int], optional): Threshold for consensus. Defaults to 50.
    """
    await generate_consensus(fasta, output, threshold)


@processAsDirectory_withThreadsIO
def threadsIO_generate_consensus(
    fasta: Path, output: Path, threshold: Optional[int] = 50
):
    """Generate consensus sequence from a fasta file.

    Args:
        fasta (Path): Path to fasta file.
        output (Path): Path to output file.
        threshold (Optional[int], optional): Threshold for consensus. Defaults to 50.
    """
    return generate_consensus_sync(fasta, output, threshold)


@processAsDirectory
async def async_pull_most_similar_taxon(
    fasta: Path, taxon_list: list[str], extract_id: str
):
    """Pull most similar taxon from a fasta file.

    Args:
        fasta (Path): Path to fasta file.
        taxon_list (list[str]): List of taxon names to search for.
        extract_id (str): Taxon id to extract.
    """
    await pull_most_similar_taxon(fasta, taxon_list, extract_id)

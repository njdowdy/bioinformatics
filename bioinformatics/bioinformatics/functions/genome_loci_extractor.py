import asyncio
import json
from dataclasses import dataclass
from pathlib import Path

from bioinformatics.functions.blast import (
    create_blast_db,
    remove_gaps,
    single_fasta_blast,
)
from bioinformatics.functions.consensus import mergeConsensusFiles, mergeFastaFiles
from bioinformatics.functions.file_utils import file_type
from bioinformatics.functions.parse_blast import (
    multiprocess_genome,
    parallel_extract_loci,
    parse_blast_hits,
    parse_reciprocal_blast_hits,
)
from bioinformatics.functions.self_blast import multiprocess_extract_loci
from bioinformatics.functions.tasks import (
    async_generate_consensus,
    async_pull_most_similar_taxon,
    generate_extraction_id,
)

# # set up constants [FOR TESTING]
# input_folder = Path("bioinformatics/input")
# query_folder = input_folder / "fasta/NOC1"
# genome_folder = Path("bioinformatics/input/reference_seqs")
# genome_file = Path("bioinformatics/input/reference_seqs/Tyria_jacobaeae_GCA_947561695.1_ilTyrJaco4.1_ChromosomeLevel_2022.fna")


@dataclass
class SimilarTaxon:
    genome: str
    similar_taxa: list[str]


# define utility functions
def is_target_file(filename: str, unique_string: str) -> bool:
    """Check if a file is a target file

    Args:
        filename (str): The name of the file
        unique_string (str): The unique string to search for in the file name

    Returns:
        bool: True if the file is a target file, False otherwise
    """
    if unique_string in filename and filename.endswith(".fasta"):
        return True
    return False


def cleanup(query_folder: Path, unique_string: str) -> None:
    """Remove all files in the query folder that contain the unique string in their name

    Args:
        unique_string (str): The unique string to search for in the file name
    """
    # List all the files in the folder
    files = query_folder.iterdir()
    # Iterate through the list of files
    for file in files:
        if is_target_file(file.name, unique_string):
            file.unlink()


def get_similar_taxa(
    genome_file: Path, similar_taxa_sets: Path = Path("")
) -> list[str]:
    """Get the most similar taxa to a genome

    Args:
        genome_file (Path): The path to the genome file. Note: filename must start with the genus and species name.
        similar_taxa_sets (Path, optional): The path to the similar taxa sets file. Defaults to "similar_taxa_sets.json" within the same folder as genome_file.

    Returns:
        list[str]: A list of the most similar taxa to the genome
    """
    # define variables
    similar_taxa = []
    if "." == str(similar_taxa_sets):
        similar_taxa_sets = genome_file.with_name("similar_taxa_sets.json")
    # get the most similar taxa to the genome
    with open(similar_taxa_sets, "r") as file:
        data_dict = json.load(file)
        # Deserialize the JSON data into a list of SimilarTaxon objects
        data = [SimilarTaxon(**item) for item in data_dict]
    # fine genome in list of similar taxa
    for entry in data:
        if (
            entry.genome.replace(" ", "_").lower()
            in genome_file.stem.replace(" ", "_").lower()
        ):
            similar_taxa = [x.replace(" ", "_") for x in entry.similar_taxa]
            break
    return similar_taxa


def extract_similar_seqs(
    genome_file: Path,
    query_folder: Path,
    similar_taxa: list[str],
    extraction_id_length: int = 10,
) -> tuple[Path, str]:
    """Extract sequences from a genome file that are similar to the taxa in the similar_taxa list

    Args:
        genome_file (Path): The path to the genome file
        similar_taxa (list[str]): A list of the similar taxa
        extraction_id_length (int, optional): The length of the extraction id. Defaults to 10.

    Returns:
        tuple[Path, str]: A tuple containing the path to the extracted sequences file and the extraction id. The extraction id will be unique based on the contents of genome_file and similar_taxa
    """
    # generate extraction id
    extraction_id = generate_extraction_id(
        f"{str(genome_file)}{''.join(similar_taxa)}", length=extraction_id_length
    )
    # extract sequences of similar taxa from all fasta files
    asyncio.run(
        async_pull_most_similar_taxon(query_folder, similar_taxa, extraction_id)
    )
    # merge all files with grouping in name
    merged_file = asyncio.run(mergeFastaFiles(query_folder, extraction_id))
    # remove any gaps from the extracted sequences
    merged_gapless_file = asyncio.run(remove_gaps(merged_file))
    # cleanup
    cleanup(query_folder, f"{extraction_id}.fasta")
    return (merged_gapless_file, extraction_id)


def extract_consensus_seqs(
    query_folder: Path, consensus_threshold: int = 50
) -> tuple[Path, str]:
    # extract consensus sequences of all taxa from all fasta files
    consensus_seqs_location = (
        Path("bioinformatics/output/consensus_seqs/")
        / Path(query_folder.parent.name)
        / Path(query_folder.name)
    )
    asyncio.run(
        async_generate_consensus(
            query_folder, consensus_seqs_location, consensus_threshold
        )
    )
    # merge consensus sequences
    merged_file_list = asyncio.run(mergeConsensusFiles(consensus_seqs_location))
    # remove any gaps from the consensus sequences
    extraction_id = f"{consensus_threshold}perc_consensus"
    merged_file = [x for x in merged_file_list if extraction_id in str(x)][0]
    merged_gapless_file = asyncio.run(remove_gaps(merged_file))
    # cleanup
    cleanup(query_folder, f"{extraction_id}.fasta")
    return (merged_gapless_file, extraction_id)


def prepare_and_create_blast_db(input: Path) -> Path:
    """Extracts the longest, most complete sequences from each FASTA file in the query folder, removes any gaps, and generates a BLAST database from these sequences

    Args:
        query_folder (Path): The path to the query folder
    """
    if input.is_dir():
        # extract the longest, most complete sequence from each fasta file in the query folder
        temp = multiprocess_extract_loci(input)
        # remove gaps from the extracted sequences
        asyncio.run(remove_gaps(temp))
        remove_temp = True
    else:
        temp = input
        remove_temp = False
    # create a blast database from these sequences
    blast_db = create_blast_db(temp)
    if remove_temp:
        # delete temp folder
        temp.unlink()
    return blast_db


def blast(
    ref: Path,
    query: Path,
    algorithm: str = "blastn",
) -> Path:
    """Blasts the query file against the reference file

    Args:
        ref (Path): The path to the reference FASTA file containing a list of sequences to blast against.
        query (Path): The path to the query file containing a list of sequences to blast against the reference file.
        algorithm (str, optional): The BLAST algorithm to use. Defaults to "blastn".

    Returns:
        Path: The path to the BLAST results file
    """
    query_name = query.stem
    reference_name = ref.stem
    # create BLAST databases
    reference_blast_db = prepare_and_create_blast_db(ref)
    # query_blast_db = prepare_and_create_blast_db(query)
    # use blastn: balanced BLAST algorithm
    blast_result_file = (
        Path("bioinformatics/output/blast_hits")
        / algorithm
        / f"{query_name}_VS_{reference_name}"
    )
    single_fasta_blast(
        query,
        reference_blast_db,
        blast_result_file,
        algorithm,
        # word_size=24,
        # mismatch_penalty=-1,
        # gapopen_penalty=2,
        # gapextend_penalty=1,
    )
    return blast_result_file


def extract_loci_from_genomes(
    query_folder: Path, genome_folder: Path, reciprocal: bool = True
):
    """Extracts loci from genomes in the genome folder that are similar to the loci in the query folder

    Args:
        query_folder (Path): The path to the query folder
        genome_folder (Path): The path to the genome folder
        reciprocal (bool, optional): Whether to perform reciprocal BLAST. Defaults to True.
    """
    # initialize some variables
    first_genome = True
    # process each genome in the reference folder
    for genome_file in genome_folder.iterdir():
        if not first_genome:
            loci_folder = Path().joinpath(
                *[
                    p if p != "input" else "output/seq_extractions"
                    for p in query_folder.parts
                ]
            )
        else:
            # # ! Why create DB from ALL sequence here? Shouldn't it be from the merged similar/consensus sequences?
            # query_blast_db = prepare_and_create_blast_db(
            #     query_folder
            # )  # create BLAST db from query sequences on first pass
            first_genome = False
        # process only fasta formatted genome files
        if file_type(genome_file) == "fasta":
            similar_taxa = get_similar_taxa(genome_file)
            # extract sequences from the query loci using similar taxa / consensus
            if similar_taxa != []:
                # extract similar taxa for blast db
                query_file, extraction_id = extract_similar_seqs(
                    genome_file, query_folder, similar_taxa
                )
            else:
                # create consensus for blast db
                query_file, extraction_id = extract_consensus_seqs(query_folder)
            #### PERFORM BLAST ####
            blast_result_file = blast(
                ref=genome_file,
                query=query_file,
            )
            # parse results of BLAST blastn on close sequences
            # blast_result_file = Path('bioinformatics/output/blast_hits/blastn/NOC1_merged_55e74d90f1_gapless_VS_Tyria_jacobaeae_GCA_947561695.1_ilTyrJaco4.1_ChromosomeLevel_2022')
            # query_folder = Path('bioinformatics/input/fasta/NOC1')
            parsed_results = parse_blast_hits(
                blast_result_file,
                query_folder,
            )
            # pull matching genome regions into their own fasta file
            # ! This part takes ~10 minutes and could use a speed up
            loci_extracted_from_ref = parallel_extract_loci(
                parsed_results, genome_folder, query_folder
            )
            if reciprocal:
                #### PERFORM RECIPROCAL BLAST ####
                # merge sequences extracted from reference together
                merged_file_list = asyncio.run(
                    mergeFastaFiles(loci_extracted_from_ref, "")
                )
                reciprocal_blast_result_file = blast(
                    ref=query_file,
                    query=merged_file_list,
                )
                # reciprocal_blast_result_file = Path('bioinformatics/output/blast_hits/blastn/NOC1_merged__VS_NOC1_merged_55e74d90f1_gapless')
                parsed_results2 = parse_blast_hits(
                    reciprocal_blast_result_file,
                    query_folder,
                )
                parse_reciprocal_blast_hits(
                    reciprocal_blast_result_file, parsed_results
                )
            #### COMPARE BLAST AND RECIPROCAL BLAST RESULTS ####
            #### MERGE PASSING RESULTS INTO ORIGINAL FASTA FILES ####
            #### CLEAN UP ALL TEMP FILES ####
            cleanup(query_folder, extraction_id)
    return Path("")

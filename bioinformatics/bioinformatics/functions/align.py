from pathlib import Path
from typing import Optional

from Bio import AlignIO, Seq

from bioinformatics.functions.file_utils import (
    create_parent_directory,
    generate_process,
    inject_parent_directory,
    replace_parent_directory,
    replace_suffix,
    suffix_parser,
)
from bioinformatics.functions.ingest import read_seq_file, write_seq_file
from bioinformatics.models.alignment import AlignmentOutputFormat, AlignmentSoftware


def pad_alignment(in_file: str, ambiguious_char: str = "-") -> None:
    suffix = suffix_parser(in_file)
    records = list(read_seq_file(in_file))
    maxlen = max(len(record.seq) for record in records)
    for record in records:
        if len(record.seq) != maxlen:
            sequence = str(record.seq).ljust(maxlen, ambiguious_char)
            record.seq = Seq.Seq(sequence)
    output_file = inject_parent_directory(in_file, "padded")
    output_file = replace_suffix(output_file, suffix)
    create_parent_directory(output_file)
    write_seq_file(records, output_file, suffix)


def align_pairwise(
    fasta: str,
):
    return None


def format_parser(in_file_suffix: str) -> str:
    if in_file_suffix in ["fa", "fas", "fasta"]:
        in_file_format = "fasta"
    elif in_file_suffix in ["phy", "phylip"]:
        in_file_format = "phylip"
    else:
        raise Exception("File format could not be parsed. Please provide.")
    return in_file_format


def convert_format(
    in_file: str,
    out_file: str,
    in_file_format: Optional[str] = None,
    out_file_format: Optional[str] = None,
) -> None:
    in_file_suffix = suffix_parser(in_file)
    if not in_file_format:
        in_file_format = format_parser(in_file_suffix)
    out_file_suffix = suffix_parser(out_file)
    if not out_file_format:
        out_file_format = format_parser(out_file_suffix)
    create_parent_directory(out_file)
    alignments = AlignIO.parse(in_file, in_file_format)
    AlignIO.write(alignments, open(out_file, "w"), out_file_format)


def perform_alignment(
    in_file: Path,
    aligner: str,
    output_format: str,
    iterations: Optional[int] = None,
    stdout: bool = True,
) -> Path:
    # in_file = 'bioinformatics/input/fasta/LEP1/L1.fa'
    # aligner = AlignmentSoftware("clustal_omega")
    # output_format = AlignmentOutputFormat("fasta")
    # iterations = 5
    out_file = in_file.with_stem(in_file.stem + "_aligned")
    out_file.parent.mkdir(parents=True, exist_ok=True)
    if not iterations:
        iterations = 1
    cmd = []
    if aligner == "clustal_omega":
        src = "bioinformatics/src/align/clustalo-1.2.4-Ubuntu-x86_64"
        cmd.extend(
            [
                src,
                "-i",
                in_file,
                "-o",
                out_file,
                "--outfmt",
                output_format,
                "--iter",
                str(iterations),
            ]
        )
    elif aligner == "muscle5":
        src = "bioinformatics/src/align/muscle5.1.linux_intel64"
        cmd.extend(
            [
                src,
                "-align",
                in_file,
                "-output",
                out_file,
                "-replicates",
                str(iterations),
            ]
        )
        if stdout:
            cmd.append("-v")
    elif aligner == "muscle3":
        src = "bioinformatics/src/align/muscle3.8.31_i86linux64"
        cmd.extend(
            [src, "-in", in_file, "-out", out_file, "-maxiters", str(iterations)]
        )
    else:
        raise Exception("Alignment software choice not understood.")
    print(f" 💪 Performing alignment of {in_file.stem}...")
    generate_process(cmd, stdout)

    return out_file

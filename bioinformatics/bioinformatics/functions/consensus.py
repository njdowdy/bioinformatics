from collections import defaultdict
from io import StringIO
from pathlib import Path
from typing import Optional

import aiofiles
from Bio import AlignIO, SeqIO
from Bio.Align import AlignInfo
from Bio.SeqIO import SeqRecord

from bioinformatics.functions.file_utils import file_type


async def mergeConsensusFiles(input_folder: Path) -> list[Path]:
    # List all files in the directory
    all_files = [f for f in input_folder.iterdir()]
    # only fasta
    all_files = [f.name for f in all_files if file_type(f) == "fasta"]
    # Group the files based on the percentage value
    file_groups = defaultdict(list)
    for file in all_files:
        if not file.endswith("perc_consensus.fasta"):
            percentage = file.split("_")[-2]
            file_groups[percentage].append(input_folder.joinpath(file))
    # Merge the files in each group
    output_files = []
    for percentage, files in file_groups.items():
        output_file = input_folder.joinpath(f"merged_{percentage}_consensus.fasta")
        async with aiofiles.open(output_file, "w") as outfile:
            for file in files:
                async with aiofiles.open(file, "r") as infile:
                    content = await infile.read()
                    await outfile.write(content)
        output_files.append(output_file)
    return output_files


async def mergeFastaFiles(input_folder: Path, groupingName: str) -> Path:
    # List all files in the directory
    all_files = [f for f in input_folder.iterdir() if groupingName in f.name]
    # only fasta files
    all_files = [f.name for f in all_files if file_type(f) == "fasta"]
    output_file = input_folder.joinpath(
        f"{input_folder.name}_merged_{groupingName}.fasta"
    )
    # Merge the files
    async with aiofiles.open(output_file, "w") as outfile:
        for file in all_files:
            async with aiofiles.open(input_folder.joinpath(file), "r") as infile:
                content = await infile.read()
                await outfile.write(content)
    return output_file


async def generate_consensus(fasta: Path, output: Path, threshold: Optional[int] = 50):
    """Generate simple consensus sequence from a fasta file
    Args:
        fasta (Path): location of fasta file to generate consensus from of the form: '<parent_dir>/<sequence_set_name>/<locus_name>.fasta'
        output (Path): location of output file of the form: '<parent_dir>/<sequence_set_name>'
        threshold (Optional[int], optional): If the percentage of the most common residue type is greater then the passed threshold, then that residue type will be used as the consensus, otherwise "N" will be added. Defaults to 50.
    """
    # do not process files with "perc_consensus" in the name
    # print(type(fasta))
    if "perc_consensus" not in fasta.name:
        if file_type(fasta) != "fasta":
            if threshold is None:
                threshold = 50
            locus_name = fasta.name.split(".")[0]
            sequence_set_name = fasta.parent.name
            output_file = output / f"{locus_name}_{threshold}perc_consensus.fasta"
            output.mkdir(parents=True, exist_ok=True)
            try:
                async with aiofiles.open(fasta, "r") as file:
                    contents = await file.read()
                contents_io = StringIO(contents)
                alignment = AlignIO.read(contents_io, "fasta")
            except Exception as e:
                print(f"An Exception occurred while procesing {fasta}: {e}")
                pass
            else:
                summary_align = AlignInfo.SummaryInfo(alignment)
                consensus = summary_align.dumb_consensus((threshold / 100), "N")
                my_seqs = SeqRecord(
                    consensus,
                    id="",
                    description=f"{sequence_set_name}_{locus_name}_{threshold}perc_consensus",
                )
                SeqIO.write(my_seqs, output_file, "fasta")


def generate_consensus_sync(fasta: Path, output: Path, threshold: Optional[int] = 50):
    """Generate simple consensus sequence from a fasta file
    Args:
        fasta (Path): location of fasta file to generate consensus from of the form: '<parent_dir>/<sequence_set_name>/<locus_name>.fasta'
        output (Path): location of output file of the form: '<parent_dir>/<sequence_set_name>'
        threshold (Optional[int], optional): If the percentage of the most common residue type is greater then the passed threshold, then that residue type will be used as the consensus, otherwise "N" will be added. Defaults to 50.
    """
    # do not process files with "perc_consensus" in the name
    if "perc_consensus" not in fasta.name:
        if threshold is None:
            threshold = 50
        locus_name = fasta.name.split(".")[0]
        sequence_set_name = fasta.parent.name
        output_file = output / f"{locus_name}_{threshold}perc_consensus.fasta"
        output.mkdir(parents=True, exist_ok=True)
        try:
            with open(fasta, "r") as file:
                contents = file.read()
            contents_io = StringIO(contents)
            alignment = AlignIO.read(contents_io, "fasta")
        except Exception as e:
            print(f"An Exception occurred while procesing {fasta}: {e}")
            pass
        else:
            summary_align = AlignInfo.SummaryInfo(alignment)
            consensus = summary_align.dumb_consensus((threshold / 100), "N")
            my_seqs = SeqRecord(
                consensus,
                id="",
                description=f"{sequence_set_name}_{locus_name}_{threshold}perc_consensus",
            )
            SeqIO.write(my_seqs, output_file, "fasta")


# !: should probably move somewhere like "extract sequences" or something, but here is fine for now
async def pull_most_similar_taxon(
    fasta: Path, taxon_list: list[str], extraction_id: str
) -> SeqRecord | None:
    """Pulls the most similar taxon from a fasta file
    Args:
        fasta (Path): location of fasta file to generate consensus from of the form: '<parent_dir>/<sequence_set_name>/<locus_name>.fasta'
        taxon_list (list[str]): list of taxon names to pull from the fasta file
        extraction_id (str): id to add to the end of the output file name
    Returns:
        SeqRecord | None: returns the most similar taxon sequence record or None if no taxon is found
    """
    # if "perc_consensus" not in fasta.name:
    if extraction_id not in fasta.name:
        if file_type(fasta) == "fasta":
            try:
                async with aiofiles.open(fasta, mode="r") as file:
                    contents = await file.read()
                alignment = list(SeqIO.parse(StringIO(contents), "fasta"))
            except:
                raise ValueError(f"Could not read alignment file: {fasta}")
            else:
                locus_name = fasta.name.split(".")[0]
                sequence_set_name = fasta.parent.name
                for taxon in taxon_list:
                    taxon_normalized = taxon.lower().replace(" ", "_")
                    for record in alignment:
                        record_id_normalized = record.id.lower().replace(" ", "_")
                        if taxon_normalized in record_id_normalized:
                            output_path = (
                                fasta.parent
                                / f"{fasta.stem}_{taxon.replace(' ', '_')}_{extraction_id}.fasta"
                            )
                            async with aiofiles.open(
                                output_path, mode="w"
                            ) as output_file:
                                await output_file.write(
                                    f">{sequence_set_name}_{locus_name}_{(record.id).split('_')[0]}_{taxon.replace(' ', '_')}\n{str(record.seq)}\n"
                                )
                            return record
                raise ValueError(
                    f"No matching taxon found in {sequence_set_name}:{locus_name}.\nTaxon list: {taxon_list}\nExamine the contents of 'similar_taxon_sets.json'"
                )

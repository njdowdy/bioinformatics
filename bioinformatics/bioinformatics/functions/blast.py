import re
import shutil
import subprocess
from enum import Enum
from io import StringIO
from pathlib import Path
from typing import Optional

import aiofiles
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqIO import SeqRecord


class BlastDataType(Enum):
    NUCL = "nucl"
    AMINO = "amino"


async def remove_gaps(input_file: Path) -> Path:
    """Remove gaps from a FASTA file
    Args:
        input_file (Path): location of the FASTA file
    """
    # Define output file name
    output_file = input_file.with_name(input_file.stem + "_gapless" + input_file.suffix)

    # Read and process sequences
    gapless_sequences = []
    async with aiofiles.open(input_file, mode="r") as fasta_file:
        fasta_data = await fasta_file.read()
        for record in SeqIO.parse(StringIO(fasta_data), "fasta"):
            # Remove gaps
            gapless_sequence = Seq(str(record.seq).replace("-", ""))
            # Create a new SeqRecord
            gapless_record = SeqRecord(
                gapless_sequence, id=record.id, description=record.description
            )
            gapless_sequences.append(gapless_record)

    # Write output file
    async with aiofiles.open(output_file, mode="w") as output_fasta:
        for gapless_record in gapless_sequences:
            await output_fasta.write(gapless_record.format("fasta"))
    return output_file


def get_blast_program(translated: bool = False) -> Path:
    """Get the path to the blast program based on the passed argument
    Args:
        translated (bool, optional): Whether the blast program should be for translated sequences. Defaults to False.
    Returns:
        Path: Path to the blast program
    """
    if not translated:
        blast_program = "bioinformatics/src/blast/ncbi-blast-2.13.0+/bin/blastn"
    else:
        blast_program = "bioinformatics/src/blast/ncbi-blast-2.13.0+/bin/tblastn"
    return Path(blast_program)


def create_blast_db(
    input_file: Path,
    output_path: Path = Path(""),
    data_type: str = "nucl",
    title="default",
) -> Path:
    """Create a blast database from a set of sequences stored in a FASTA file
    Args:
        input_file (Path): location of the set sequences (e.g., "bioinformatics/input/fasta/<setName>/merged_XXperc_consensus.fasta")
        output_path (Optional[Path], optional): location to output blast database. Defaults to None, which places files at e.g., "bioinformatics/output/blastdb/<title>/<title>".
        data_type (Optional[str], optional): data type of consensus sequence set. Defaults to "nucl".
        title (str, optional): title of the blast database. Defaults to "default".
    """
    try:
        parsed_data_type = BlastDataType[data_type.upper()]
    except KeyError:
        raise ValueError(
            f"Invalid data type '{data_type}'. Options are {', '.join([datatype.value for datatype in BlastDataType])}"
        )
    if "." == str(output_path):
        title = (
            input_file.name.replace(".fasta", "")
            .replace(".fas", "")
            .replace(".fa", "")
            .replace(".fna", "")
        )
        output_path = Path(f"bioinformatics/output/blastdb/{title}/{title}")
    cmd = [
        "bioinformatics/src/blast/ncbi-blast-2.13.0+/bin/makeblastdb",
        "-in",
        str(input_file),
        "-parse_seqids",
        "-dbtype",
        parsed_data_type.value,
        "-out",
        str(output_path),
        "-title",
        title,
    ]
    print(f"🧨 Creating BLAST database at {output_path}...")
    subprocess.run(
        cmd,
    )
    return output_path


class BlastAlgorithm(Enum):
    MEGABLAST = "megablast"
    DCMEGABLAST = "dc-megablast"
    BLASTN = "blastn"


def single_fasta_blast(
    fasta_query_path: Path,
    blastdb_path: Path,
    output_path: Path,
    algorithm: str = "blastn",
    translated: bool = False,
    word_size: Optional[int] = None,
    mismatch_penalty: Optional[int] = None,
    gapopen_penalty: Optional[int] = None,
    gapextend_penalty: Optional[int] = None,
) -> None:
    """Run a BLAST search on a single fasta file against a blast database
    Args:
        fasta_query_path (Path): location of fasta file to BLAST against the blast database
        blastdb_path (Path): location of blast database, e.g., "bioinformatics/output/blastdb/<setName>"
        output_path (Optional[Path], optional): location to output blast results. Defaults to None.
        translated (bool, optional): whether or not the fasta file is translated. Defaults to False.
        algorithm (str, optional): blast algorithm to use. Defaults to "blastn". Options are "megablast", "dc-megablast", and "blastn".
        word_size (Optional[int], optional): word size to use for megablast. Defaults to None.
        mismatch_penalty (Optional[int], optional): mismatch penalty to use for megablast. Defaults to None.
        gapopen_penalty (Optional[int], optional): gap open penalty to use for megablast. Defaults to None.
        gapextend_penalty (Optional[int], optional): gap extend penalty to use for megablast. Defaults to None.
    """
    try:
        parsed_algo = BlastAlgorithm[algorithm.upper().replace("DC-MEGA", "DCMEGA")]
    except KeyError:
        raise ValueError(
            f"Invalid algorithm '{algorithm}'. Options are {', '.join([algo.value for algo in BlastAlgorithm])}"
        )

    blast_program = get_blast_program(translated)

    output_path.parent.mkdir(parents=True, exist_ok=True)

    blastdb_path = blastdb_path

    cmd = [
        str(blast_program),
        "-db",
        str(blastdb_path),
        "-query",
        str(fasta_query_path),
        "-task",
        parsed_algo.value,
        "-out",
        str(output_path),
        "-outfmt",
        "5",
    ]

    # special options
    if word_size is not None:
        cmd.extend(["-word_size", str(word_size)])
    if mismatch_penalty is not None:
        cmd.extend(["-penalty", str(mismatch_penalty)])
    if gapopen_penalty is not None:
        cmd.extend(["-gapopen", str(gapopen_penalty)])
    if gapextend_penalty is not None:
        cmd.extend(["-gapextend", str(gapextend_penalty)])

    print(f"💥 Initiating BLAST of database at {blastdb_path}...")

    if cmd is not None:
        subprocess.run(
            cmd,
        )


def copyLociMissingHits(input_fasta_path: Path, output_fasta_path: Path) -> None:
    """Copy the .fasta files from one directory to another if they are missing from the second directory

    Args:
        input_fasta_path (Path): The path to the directory containing the .fasta files
        output_fasta_path (Path): The path to the directory to copy the .fasta files to
    """
    # List all the .fasta files in both directories
    fasta_files_dir1 = [
        f.name
        for f in input_fasta_path.iterdir()
        if f.is_file() and f.name.endswith(".fasta") and not "perc_consensus" in f.name
    ]
    fasta_files_dir2 = [
        f.name
        for f in output_fasta_path.iterdir()
        if f.is_file() and f.name.endswith(".fasta")
    ]

    # Find the missing .fasta files
    missing_files = set(fasta_files_dir1) - set(fasta_files_dir2)

    # Copy the missing .fasta files from dir1 to dir2
    for file in missing_files:
        src_path = input_fasta_path / file
        dest_path = output_fasta_path / file
        shutil.copy(src_path, dest_path)
        print(
            f"Copied the missing {file} from {input_fasta_path} to {output_fasta_path}"
        )


def listLociMissingHits(
    input_fasta_path: Path,
    output_fasta_path: Path,
    groupingName: str,
) -> set[str]:
    """List the .fasta files that are missing from one directory compared to another

    Args:
        input_fasta_path (Path): The path to the directory containing the .fasta files
        output_fasta_path (Path): The path to the directory to copy the .fasta files to
        groupingName (str, optional): The name of the grouping to use. Defaults to "perc_consensus".

    Returns:
        set[str]: A set of the missing .fasta files
    """
    # List all the .fasta files in both directories
    fasta_files_dir1 = [
        f.name
        for f in input_fasta_path.iterdir()
        if f.is_file() and re.match(r"L[0-9]+\.(fasta|fa|fna|fas)", f.name)
    ]
    fasta_files_dir2 = [
        f.name
        for f in output_fasta_path.iterdir()
        if f.is_file() and re.match(r"L[0-9]+\.(fasta|fa|fna|fas)", f.name)
    ]

    # Find the missing .fasta files
    missing_files = set(fasta_files_dir1) - set(fasta_files_dir2)

    return missing_files

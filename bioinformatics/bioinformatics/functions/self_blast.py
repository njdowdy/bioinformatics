import asyncio
from collections import Counter
from concurrent.futures import ProcessPoolExecutor, as_completed
from itertools import zip_longest
from pathlib import Path

from Bio import SeqIO
from Bio.SeqIO import SeqRecord
from tqdm import tqdm

from bioinformatics.functions.align import perform_alignment
from bioinformatics.functions.blast import (
    create_blast_db,
    remove_gaps,
    single_fasta_blast,
)
from bioinformatics.functions.parse_blast import parse_self_blast_hits

##### UTILITY FUNCTIONS #####


def count_ambiguities(seq):
    ambiguities = ["R", "Y", "S", "W", "K", "M", "B", "D", "H", "V", "N", "-"]
    return sum(seq.count(ambiguity) for ambiguity in ambiguities)


def extract_longest_least_ambiguous_sequence(
    fasta_file: Path,
    output_file: Path,
) -> None:
    with fasta_file.open("r") as file:
        sequences = list(SeqIO.parse(file, "fasta"))
        # Sort by length (descending) and ambiguity count (ascending)
        sorted_sequences = sorted(
            sequences,
            key=lambda seq: (
                -len(seq) + seq.seq.count("-"),
                count_ambiguities(seq.seq),
            ),
        )
        longest_least_ambiguous_seq = sorted_sequences[0]
        new_seq_id = f"{fasta_file.stem}_{'_'.join(longest_least_ambiguous_seq.id.split(' ')[0].split('_')[0:2])}"
        new_seq_record = SeqRecord(
            longest_least_ambiguous_seq.seq, id=new_seq_id, description=""
        )
        with output_file.open("a") as output:
            SeqIO.write(new_seq_record, output, "fasta")


def multiprocess_extract_loci(
    query_folder: Path,
) -> Path:
    """Perform the fasta operations in parallel

    Args:
        instruction_file (Path): The path to the instruction file
        ref_folder (Path): The path to the folder containing the reference files
        query_folder (Path): The path to the folder containing the query files
    """
    output_file = Path(
        f"{str(query_folder).replace('input/fasta','output/temp')}/extract.fasta"
    )
    output_file.parent.mkdir(parents=True, exist_ok=True)
    fasta_files = [
        file
        for file in query_folder.iterdir()
        if file.is_file() and file.suffix == ".fasta"
    ]
    with ProcessPoolExecutor() as executor:
        futures = [
            executor.submit(
                extract_longest_least_ambiguous_sequence, fasta_file, output_file
            )
            for fasta_file in fasta_files
        ]
        for future in tqdm(
            as_completed(futures), total=len(futures), desc="Processing"
        ):
            pass
    return output_file


def generate_fasta_paths(base_path: Path, file_names: list[str]) -> list[Path]:
    return [base_path.joinpath(f"{file_name}.fasta") for file_name in file_names]


def extract_sequence_labels(fasta_paths: list[Path]) -> list[str]:
    sequence_labels = set()
    for path in fasta_paths:
        with path.open() as fasta_file:
            for line in fasta_file:
                if line.startswith(">"):
                    label = line.strip().split()[0][1:]
                    sequence_labels.add(label)
    return list(sequence_labels)


def read_fasta(file: Path):
    sequences = {}
    with file.open("r") as f:
        seq_name = None
        seq_data = ""
        for line in f:
            line = line.strip()
            if line.startswith(">"):
                if seq_name is not None:
                    sequences[seq_name] = seq_data
                seq_name = line[1:]
                seq_data = ""
            else:
                seq_data += line
        if seq_name is not None:
            sequences[seq_name] = seq_data
    return sequences


def write_combined_fasta(
    sequences: list[str],
    names: list[str],
    output: Path,
    mode: str = "w",
    uniqueLabels: bool = False,
) -> None:
    with output.open(mode) as f:
        if uniqueLabels:
            for i, (name, sequence) in enumerate(zip(names, sequences)):
                f.write(f">seq{i + 1}_{name}\n{sequence}\n")
        else:
            for (name, sequence) in zip(names, sequences):
                f.write(f">{name}\n{sequence}\n")


IUPAC_AMBIGUOUS_DNA = {
    "A": "A",
    "C": "C",
    "G": "G",
    "T": "T",
    "R": "AG",
    "Y": "CT",
    "S": "GC",
    "W": "AT",
    "K": "GT",
    "M": "AC",
    "B": "CGT",
    "D": "AGT",
    "H": "ACT",
    "V": "ACG",
    "N": "ACGT",
}

# Create the reverse mapping for IUPAC_AMBIGUOUS_DNA
IUPAC_AMBIGUOUS_DNA_REVERSE = {v: k for k, v in IUPAC_AMBIGUOUS_DNA.items()}


def resolve_ambiguous_bases(column_bases: list[str], threshold: float = 0.60) -> str:
    column_bases = [base.upper() for base in column_bases]
    unambiguous_bases = [base for base in column_bases if base in "ACGT"]
    unambiguous_count = Counter(unambiguous_bases)
    if not unambiguous_bases:
        unique_bases = set(
            "".join([IUPAC_AMBIGUOUS_DNA[base] for base in column_bases])
        )
        consensus_base = "".join(sorted(unique_bases))
        return IUPAC_AMBIGUOUS_DNA_REVERSE.get(consensus_base, "N")
    for base, count in unambiguous_count.items():
        if count / len(unambiguous_bases) >= threshold:
            return base
    unique_bases = set(
        "".join([IUPAC_AMBIGUOUS_DNA[base] for base in unambiguous_bases])
    )
    consensus_base = "".join(sorted(unique_bases))
    return IUPAC_AMBIGUOUS_DNA_REVERSE.get(consensus_base, "N")


def create_merged_sequence(aligned_sequence_list: list[str]) -> str:
    # Extract only the sequences and ignore sequence names
    merged_sequence = []
    for column in zip_longest(*aligned_sequence_list, fillvalue="-"):
        column_bases = [base for base in column if base != "-"]
        if not column_bases:
            merged_sequence.append("-")
        else:
            consensus_base = resolve_ambiguous_bases(column_bases)
            merged_sequence.append(consensus_base)
    return "".join(merged_sequence)


def realign_merged_files(file_list: list[Path]) -> None:
    for file in file_list:
        perform_alignment(file, "muscle5", "fasta")
        # Create a Path object for the new file with the desired name
        new_file = file.with_name(f"{'_'.join(file.stem.split('_')[0:-1])}.fasta")
        # Rename the original file to the new name
        file.rename(new_file)


def merge_self_blast_results(
    results: list[list[str]], query_folder: Path
) -> list[Path]:
    output_file_list = []
    for result in results:
        files = generate_fasta_paths(query_folder, result)
        names = extract_sequence_labels(files)
        output_filename = "_".join([file.stem for file in files]) + ".fasta"
        output_file = query_folder / output_filename
        counter = 0
        # append locus name to tip labels
        for file in files:
            sequences = read_fasta(file)
            # remove gaps from sequences
            sequences = {
                name: sequence.replace("-", "") for name, sequence in sequences.items()
            }
            for name in names:
                if name in sequences:
                    sequences[f"{name}_{file.stem}"] = sequences[name]
                    del sequences[name]
            if counter == 0:
                mode = "w"
            else:
                mode = "a"
            write_combined_fasta(
                list(sequences.values()),
                list(sequences.keys()),
                output_file,
                mode=mode,
                uniqueLabels=False,
            )
            counter += 1
        # perform alignment of combined gapless file
        print(f"▶️ Starting to align {output_file.stem}. This might take a while!")
        aligned_file = perform_alignment(output_file, "clustal_omega", "fasta")
        # clean up temp files
        output_file.unlink()
        sequences = read_fasta(aligned_file)
        counter = 0
        for name in names:
            sequence_list = []
            # extract sequences from same taxon
            for key, value in sequences.items():
                if name in key:
                    sequence_list.append(value)
            # merge sequences
            merged_sequence = create_merged_sequence(sequence_list)
            # write merged sequence to file
            if counter == 0:
                mode = "w"
            else:
                mode = "a"
            with output_file.open(mode) as fileio:
                fileio.write(f">{name}\n{merged_sequence}\n")
            counter += 1
        # clean up temp files
        aligned_file.unlink()
        # append output file to list
        output_file_list.append(output_file)
        # change input file extensions to backup
        for file in files:
            file.rename(file.with_suffix(".backup"))
    return output_file_list


##### MAIN FUNCTION #####


def perform_self_blast(
    query_folder: Path,
    output_folder: Path = Path("bioinformatics/output"),
    blast_algorithm: str = "megablast",
) -> None:
    # extract the longest, most complete sequence from each fasta file in the query folder
    temp = multiprocess_extract_loci(query_folder)
    # remove gaps from the extracted sequences
    temp_gapless = asyncio.run(remove_gaps(temp))
    # delete temp folder
    temp.unlink()
    # create a blast database from extracted query sequences
    create_blast_db(temp_gapless)
    # perform blast search
    blast_result_file = (
        output_folder
        / "blast_hits"
        / blast_algorithm
        / f"{query_folder.stem}_VS_{query_folder.stem}"
    )
    single_fasta_blast(
        temp_gapless,
        output_folder / "blastdb" / f"{query_folder.stem}__{temp_gapless.stem}",
        blast_result_file,
        blast_algorithm,
    )
    results = parse_self_blast_hits(blast_result_file)
    if results:
        merged_alignments = merge_self_blast_results(results, query_folder)
        realign_merged_files(merged_alignments)

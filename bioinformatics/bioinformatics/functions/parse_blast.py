import csv
import re
from concurrent.futures import ProcessPoolExecutor, as_completed
from pathlib import Path
from xml.etree.ElementTree import Element, fromstring

from Bio import SeqIO
from Bio.SeqIO import SeqRecord
from tqdm import tqdm


def find_longest_sequence(fasta_file: Path) -> int:
    """Find the longest sequence in a fasta file
    Args:
        fasta_file (Path): location of the fasta file
    Returns:
        int: length of the longest sequence
    """
    longest_length = 0
    with open(fasta_file, "r") as file:
        for record in SeqIO.parse(file, "fasta"):
            seq_length = len(record.seq)
            if seq_length > longest_length:
                longest_length = seq_length

    return longest_length


def get_algorithm(xml_file_path: Path) -> str:
    """Get the algorithm used to generate the blast results
    Args:
        xml_file_path (Path): location of the blast XML file
    Returns:
        str: blast algorithm
    """
    if "dcmegablast" in xml_file_path.name:
        return "dcmegablast"
    elif "megablast" in xml_file_path.name:
        return "megablast"
    else:
        return "blastn"


def write_csv_header(output_file: Path, header: list[str]) -> None:
    """Write the header to the output file
    Args:
        output_file (Path): location of the output file
        header (list[str]): list of header names
    """
    with open(output_file, "w", newline="") as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=header)
        writer.writeheader()


def sort_and_write_sorted_csv(output_file: Path) -> None:
    """Sort the csv file and write the sorted data to the file
    Args:
        output_file (Path): location of the output file
    """
    with open(output_file, "r") as csvfile:
        reader = csv.DictReader(csvfile)
        header = reader.fieldnames
        data = [row for row in reader]
        sorted_data = sorted(
            data, key=lambda row: (row["hit"].lower() == "true", row["locus"])
        )
        if header is not None:
            with open(output_file, "w", newline="") as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=header)
                writer.writeheader()
                writer.writerows(sorted_data)


def parse_blast_hits(xml_file_path: Path, query_folder: Path) -> Path:
    """Parse the blast hits from the XML file
    Args:
        xml_file_path (Path): location of the blast XML file
        query_folder (Path): path to the query fasta files

    Returns:
        Path: location of the parsed blast results
    """
    print(f"📋 Parsing BLAST results...")
    # Read the XML data from the file
    with open(xml_file_path, "r") as file:
        xml_data = file.read()
    root = fromstring(xml_data)
    # get algorithm that was used
    algorithm = get_algorithm(xml_file_path)
    # group name
    reference_name = xml_file_path.name.split("_VS_")[1]
    # make output file
    output_folder = Path("bioinformatics/output/parsed_blast_results") / algorithm
    output_folder.mkdir(parents=True, exist_ok=True)
    output_file = output_folder / f"{xml_file_path.name}.csv"
    header = [
        "reference",
        "query",
        "locus",
        "hit",
        "hit_name",
        "ref_start",
        "ref_end",
        "reversed_hit_frame",
        "best_hsps_group_score",
        "hsps_count",
        "hsps_repr",
    ]
    # Write the header and data to the output file
    write_csv_header(output_file, header)
    # initialize some variables so they will not be unbounded
    set_name = ""
    locus = ""
    locus_length = 0
    # Iterate through the hits
    for iteration in tqdm(root.findall(".//Iteration"), desc="Parsing hits"):
        query_seq = iteration.find("Iteration_query-def")
        if query_seq is not None:
            query_seq_name = query_seq.text
            if query_seq_name is not None:
                # could be like "NOC1_L26_I4260_Haploa" or "NOC1_L356_L357_I4260_Haploa"
                locus = "_".join(
                    [x for x in query_seq_name.split("_") if re.match(r"^L[0-9]*", x)]
                )
                set_name = query_seq_name.split(f"_{locus}")[0]
                # get the longest sequence length from the locus fasta file
                fasta_file = query_folder / f"{locus}.fasta"
                locus_length = find_longest_sequence(fasta_file)
        top_score = 0  # Initialize the best_hsps_group_score before the loop
        for iteration_hit in iteration.findall(".//Iteration_hits"):
            # initialize some variables so they will not be unbounded
            hit_state = False
            reversed_hit_frame = ""
            buffered_extract_start = ""
            buffered_extract_end = ""
            hsps_repr = ""
            db_hit_name = ""
            hsps_count = 0
            best_hsps_group_score = ""
            hit_count = 0
            hits = iteration_hit.findall(".//Hit")
            if hits != []:
                hit_state = True
                for hit in hits:
                    hit_count += 1
                    db_hit = hit.find("Hit_def")
                    db_accession = hit.find("Hit_accession")
                    db_hit_text = ""
                    if db_hit is not None:
                        db_hit_text = f"_{db_hit.text}"
                        if db_hit_text == "_No definition line":
                            db_hit_text = ""
                    db_accession_text = ""
                    if db_accession is not None:
                        db_accession_text = db_accession.text
                    hsps_count = 0
                    for hit_hsps in hit.findall("Hit_hsps"):
                        hsps_count += 1
                        hsp_list = hit_hsps.findall(".//Hsp")
                        hsps = parse_hsps_data(hsp_list)
                        best_hsps_group = findContiguousHsps(hsps)
                        if (
                            best_hsps_group is not None
                            and best_hsps_group[3] > top_score
                        ):
                            # check that best_hsps_group score is better than previous best score
                            top_score = best_hsps_group[3]
                            # extract the data from the best hsps group from the hit containing the best score so far
                            buffered_data = bufferExtractSegment(
                                best_hsps_group, 0.2, 100, locus_length
                            )
                            buffered_extract_start = buffered_data[0]
                            buffered_extract_end = buffered_data[1]
                            reversed_hit_frame = buffered_data[2] != 1
                            hsps_repr = buffered_data[4]
                            best_hsps_group_score = best_hsps_group[3]
                            db_hit_name = f"{db_accession_text}{db_hit_text}"
            else:
                hit_state = False
                reversed_hit_frame = False
                buffered_extract_start = ""
                buffered_extract_end = ""
                hsps_repr = ""
                db_hit_name = ""
                hsps_count = 0
                best_hsps_group_score = ""
            row_data = {
                "reference": reference_name,
                "query": set_name,
                "locus": locus,
                "hit": hit_state,
                "hit_name": db_hit_name,
                "ref_start": buffered_extract_start,
                "ref_end": buffered_extract_end,
                "reversed_hit_frame": reversed_hit_frame,
                "best_hsps_group_score": best_hsps_group_score,
                "hsps_count": hsps_count,
                "hsps_repr": hsps_repr,
            }
            # Write the data to the output file
            with open(output_file, "a", newline="") as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=header)
                writer.writerow(row_data)
    # sort and write csv file by hit_state
    sort_and_write_sorted_csv(output_file)
    return output_file


def extract_taxon(taxon: str, files: list[Path]) -> Path | None:
    for file in files:
        with file.open() as fasta_file:
            for line in fasta_file:
                if line.startswith(">"):
                    label = line.strip().split()[0][1:]
                    if taxon in label:
                        return file
                    else:
                        raise ValueError(f"Taxon {taxon} not found in {file}")


def traverse_graph(
    key: str, input_dict: dict[str, list[str]], visited_keys: set[str], group: list[str]
):
    if key not in visited_keys:
        visited_keys.add(key)
        group.append(key)
        for value in input_dict[key]:
            traverse_graph(value, input_dict, visited_keys, group)


def process_dict(input_dict: dict[str, list[str]]) -> list[list[str]]:
    visited_keys = set()
    result_groups = []
    for key in input_dict:
        if key not in visited_keys:
            group = []
            traverse_graph(key, input_dict, visited_keys, group)
            result_groups.append(group)
    return result_groups


def parse_self_blast_hits(xml_file_path: Path) -> list[list[str]]:
    print(f"📋 Parsing Reciprocal BLAST results...")
    # Read the XML data from the file
    with open(xml_file_path, "r") as file:
        xml_data = file.read()
    root = fromstring(xml_data)
    results = {}
    for iteration in root.findall(".//Iteration"):
        query_locus = iteration.find("Iteration_query-def")
        if query_locus is not None:
            query_locus = query_locus.text
            if query_locus is not None:
                query_locus = query_locus.split("_")[0]
        for iteration_hit in iteration.findall(".//Iteration_hits"):
            hits = iteration_hit.findall(".//Hit")
            if len(hits) > 1:
                print(f"🚨 Multiple hits found for {query_locus}")
                off_target_hits = []
                for hit in hits:
                    name = hit.find("Hit_id")
                    if name is not None and name.text is not None:
                        off_target_hits.append(name.text.split("_")[0])
                off_target_hits = [x for x in off_target_hits if x != query_locus]
                results[query_locus] = off_target_hits
    result = process_dict(results)
    return result


def parse_reciprocal_blast_hits(xml_file_path: Path, instruction_file: Path) -> None:
    print(f"📋 Parsing Reciprocal BLAST results...")
    # Read the XML data from the file

    with open(xml_file_path, "r") as file:
        xml_data = file.read()
    root = fromstring(xml_data)
    locus = set()
    paralog_hit_count = {}
    # open the csv instruction_file, get a list of all unique values in the field "locus", and put in dictionary with value = 0
    with open(instruction_file, "r") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            locus.add(row["locus"])
    for l in list(locus):
        paralog_hit_count[l] = 0
    sorted_keys = sorted(paralog_hit_count.keys())
    paralog_hit_count = {key: paralog_hit_count[key] for key in sorted_keys}

    # for iteration in tqdm(root.findall(".//Iteration"), desc="Parsing hits"):
    for iteration in root.findall(".//Iteration"):
        iteration_name = Element("None") or iteration.find("Iteration_query-def")
        if iteration_name is not None and iteration_name.text is not None:
            iteration_locus = re.findall(r"(L[0-9]*)_", iteration_name.text)
            if len(iteration_locus) == 0:
                raise ValueError(f"Could not parse locus from {iteration_locus}")
            else:
                iteration_locus = "_".join(iteration_locus)
        else:
            iteration_locus = ""
        for iteration_hit in iteration.findall(".//Iteration_hits"):
            for hit in iteration_hit.findall(".//Hit"):
                hit_locus = hit.find("Hit_accession")
                if hit_locus is not None and hit_locus.text is not None:
                    hit_locus = re.findall(r"(L[0-9]*)_", hit_locus.text)
                    if len(hit_locus) == 0:
                        raise ValueError(f"Could not parse locus from {hit_locus}")
                    else:
                        hit_locus = "_".join(hit_locus)
                db_hit = hit.find("Hit_id")
                if db_hit and db_hit.text:
                    kit_locus = db_hit.text.split("_")[
                        0
                    ]  #! this field should start with kit name
                for hit_hsps in hit.findall("Hit_hsps"):
                    hsp_list = hit_hsps.findall(".//Hsp")
                    for hsp in hsp_list:
                        hsp_bitscore = hsp.find("Hsp_bit-score")
                        if hsp_bitscore is not None and hsp_bitscore.text is not None:
                            hsp_score = int(float(hsp_bitscore.text))
                            if hsp_score > 69:
                                if hit_locus != iteration_locus:
                                    print(
                                        f"🚨 {iteration_locus} hit {hit_locus} with score {hsp_score}"
                                    )
                                    paralog_hit_count[hit_locus] = (
                                        paralog_hit_count.get(hit_locus, 0) + 1
                                    )
                                # TODO WHY DIDN'T SELF BLAST FIND THESE?
                                # THESE ARE PROBABLY PARALOGS AND WE SHOULD JUST
                                # PICK THE ONE THAT IS THE LONGEST,
                                # LEAST AMBIGUOUS VERSION OF THE SET
                                #! 🚨 L615 hit L812 with score 517
                                #! 🚨 L795 hit L757 with score 94
                                #! 🚨 L812 hit L615 with score 354
                                #! 🚨 L89 hit L88 with score 111
                                #! 🚨 L290 hit L291 with score 93
                                #! 🚨 L443 hit L444 with score 140
                                #! 🚨 L274 hit L275 with score 90
                                #! 🚨 L405 hit L406 with score 400
                                #! 🚨 L406 hit L405 with score 99
                                #! 🚨 L88 hit L89 with score 88
                                #! 🚨 L275 hit L274 with score 70
                                #! 🚨 L444 hit L443 with score 139
                                #! 🚨 L757 hit L795 with score 153

    # which loci do not have any hits?
    paralogs = {key: value for key, value in paralog_hit_count.items() if value != 0}
    # zero values can be from multiple low-scoring hsps that need to be summed, or perhaps we can check if there are multiple hsps and adjust the threshold


def bufferExtractSegment(
    segment: tuple[int, int, int, int, str],
    bufferPerc: float = 0.2,
    bufferBp: int = 100,
    locus_length: int = 0,
) -> tuple[int, int, int, int, str]:
    """Buffer the start and end of a segment by a percentage of the segment's length.

    Args:
        segment (tuple[int, int, int, str]): A tuple containing the start, end, and direction of a segment.
        bufferPerc (float, optional): The percentage of the segment's length to buffer. Defaults to 0.1.
        bufferBp (int, optional): The minimum number of base pairs to buffer. Defaults to 100.
        locus_length (int, optional): The length of the locus. Defaults to 0.

    Returns:
        tuple[int, int, int, str]: A tuple containing the buffered start, end, and direction of a segment.
    """
    segment_start = segment[0]
    segment_end = segment[1]
    segment_length = (segment_end - segment_start) + 1
    segment_direction = segment[2]
    score = segment[3]
    adj = int(bufferPerc * segment_length)
    if adj < bufferBp:
        adj = bufferBp
    start_adjustment = adj if adj < segment_start else segment_start - 1
    end_adjustment = adj  # TODO: buffered_segment_end + adj should not be longer than the db sequence's total length
    buffered_segment_start = segment_start - start_adjustment
    buffered_segment_end = segment_end + end_adjustment
    buffered_length = (buffered_segment_end - buffered_segment_start) + 1
    # check that the buffered length meets or exceeds the locus maximum length
    if locus_length > 0:
        if buffered_length < locus_length:
            # if the buffered length is less than the locus maximum length, expand the buffer to meet the locus maximum length
            buffer_difference = locus_length - buffered_length
            start_adjustment += buffer_difference // 2
            end_adjustment += buffer_difference // 2
            buffered_segment_start = segment_start - start_adjustment
            buffered_segment_end = segment_end + end_adjustment
    # adjust strRepl of segments and gaps
    if "-" in segment[4]:
        strRepl_temp = segment[4].split("-")
        new_first_segment_length = int(strRepl_temp[0]) + start_adjustment
        new_last_segment_length = int(strRepl_temp[-1]) + end_adjustment
        strRepl = f"{new_first_segment_length}-{'-'.join(strRepl_temp[1:-1])}-{new_last_segment_length}".replace(
            "--", "-"
        )
    else:
        # when there are no gaps, we need to expand both sides of the single segment
        strRepl = f"{int(segment[4]) + start_adjustment + end_adjustment}"
    return (
        buffered_segment_start,
        buffered_segment_end,
        segment_direction,
        score,
        strRepl,
    )


def parse_hsps_data(hsps_list: list[Element]) -> list[tuple[int, int, int, int]] | list:
    """Parse the hsps data from the blast xml file and return a list of tuples containing the start and end positions of the hsps

    Args:
        hsps_list (list[Element]): A list of the hsps elements from the blast xml file

    Returns:
        list[tuple[int, int, int, int]] | list: A list of tuples containing the start and end positions of the hsps
    """
    parsed_hsps = []
    for hsp in hsps_list:
        hsp_start_loc = hsp.find("Hsp_hit-from")
        hsp_end_loc = hsp.find("Hsp_hit-to")
        hsp_score = hsp.find("Hsp_bit-score")
        hsp_frame = hsp.find("Hsp_hit-frame")
        if (
            hsp_start_loc is not None
            and hsp_end_loc is not None
            and hsp_score is not None
            and hsp_frame is not None
        ):
            hsp_start_loc = hsp_start_loc.text
            hsp_end_loc = hsp_end_loc.text
            hsp_score = hsp_score.text
            hsp_frame = hsp_frame.text
            if (
                hsp_start_loc is not None
                and hsp_end_loc is not None
                and hsp_score is not None
                and hsp_frame is not None
            ):
                if int(hsp_frame) < 0:
                    hsp_start_loc, hsp_end_loc = hsp_end_loc, hsp_start_loc
                parsed_hsps.append(
                    (
                        int(hsp_start_loc),
                        int(hsp_end_loc),
                        int(float(hsp_score)),
                        int(hsp_frame),
                    )
                )
    return parsed_hsps


def createHspsStrRepl(hsps_list: list[tuple[int, int, int, int]]) -> str:
    """Create a string representation of the hsps

    Args:
        hsps_list (list[tuple[int, int, int, int]]): A list of tuples containing the start and end positions of the hsps

    Returns:
        str: A string representation of the hsps
    """
    result = []
    if len(hsps_list) == 0:
        return ""
    elif len(hsps_list) == 1:
        return str(hsps_list[0][1] - hsps_list[0][0])
    else:
        for i in range(len(hsps_list)):
            if i == 0:
                result.append(str(hsps_list[i][1] - hsps_list[i][0]))
            else:
                gap = hsps_list[i][0] - hsps_list[i - 1][1] - 1
                if gap > 0:
                    result.append(f"[{gap}]")
                result.append(str(hsps_list[i][1] - hsps_list[i][0]))
        return "-".join(result)


def findContiguousHsps(
    hsps: list[tuple[int, int, int, int]],
    distanceThreshold: int = 1000,
    scoreThreshold: int = 68,
) -> tuple[int, int, int, int, str] | None:
    """Find the contiguous hsps with the highest score

    Args:
        hsps (list[tuple[int, int, int, int]]): A list of tuples containing the start and end positions of the hsps
        distanceThreshold (int, optional): The maximum distance between two hsps to be considered contiguous. Defaults to 1000.
        scoreThreshold (int, optional): The minimum score for a hsp to be considered. Defaults to 70.

    Returns:
        tuple[int, int, int, str] | None: A tuple containing the start, end, direction, and string representation of the contiguous hsps with the highest score
    """
    # Sort the line segments by their starting positions
    hsps.sort(key=lambda x: x[0])
    # Initialize clusters
    clusters = []
    # Iterate through the sorted line segments
    for seg in hsps:
        # Check if the current segment meets the score threshold
        if seg[2] >= scoreThreshold:
            # If there are no clusters yet, create a new cluster with the current segment
            if not clusters:
                clusters.append([seg])
            else:
                # If the current segment is within the distance cutoff from the last cluster, add it
                if seg[0] - clusters[-1][-1][1] <= distanceThreshold:
                    clusters[-1].append(seg)
                else:
                    clusters.append([seg])
    # Initialize the top cluster and highest score
    top_cluster = None
    highest_score = 0
    top_scoring_segment = None
    # Iterate through each cluster
    for cluster in clusters:
        # Find the top-scoring line segment in the current cluster
        current_top_scoring_segment = max(cluster, key=lambda x: x[2])
        # Update the top cluster if the top-scoring segment has a higher score than the current highest score
        if current_top_scoring_segment[2] > highest_score:
            highest_score = current_top_scoring_segment[2]
            top_scoring_segment = current_top_scoring_segment
            top_cluster = cluster
    if top_cluster is not None and top_scoring_segment is not None:
        # Filter the line segments in the top cluster based on the "frame" factor of the highest-scoring segment
        filtered_segments = [
            seg for seg in top_cluster if seg[3] == top_scoring_segment[3]
        ]
        segment_repr = createHspsStrRepl(filtered_segments)
        # Merge the filtered line segments into one contiguous line segment
        min_start = min(filtered_segments, key=lambda x: x[0])[0]
        max_end = max(filtered_segments, key=lambda x: x[1])[1]
        frame = top_scoring_segment[3]
        score = top_scoring_segment[2]
        # The merged line segment
        merged_segment = (min_start, max_end, frame, score, segment_repr)
    else:
        merged_segment = None
    return merged_segment


def replace_subdir(path: Path, target_subdir: str, new_subdir: str) -> Path:
    parts = path.parts
    target_index = parts.index(target_subdir)
    new_parts = parts[:target_index] + (new_subdir,)
    return Path(*new_parts)


def extract_sequence(
    row: dict[str, str],
    ref_folder: Path,
    query_folder: Path,
) -> Path:
    """Extract the sequence from the reference file and write it to the output file

    Args:
        row (dict[str, str]): A dictionary containing the information about the sequence to be extracted
        ref_folder (Path): The path to the folder containing the reference files
        query_folder (Path): The path to the folder containing the query files
    """
    reference_file = (
        ref_folder / f"{row['reference']}.fna"
    )  # TODO: allow file types other than fna
    original_file = (
        query_folder / f"{row['locus']}.fasta"
    )  # TODO: allow file types other than fasta
    output_folder = replace_subdir(
        query_folder,
        "input",
        f"output/seq_extractions/{row['reference']}/{row['query']}",
    )
    output_file = output_folder / f"{row['locus']}.fasta"
    output_file.parent.mkdir(parents=True, exist_ok=True)
    ref_start = int(row["ref_start"])
    ref_end = int(row["ref_end"])
    reversed_hit_frame = row["reversed_hit_frame"].lower() == "true"
    ref_name = row["hit_name"]
    for record in SeqIO.parse(reference_file, "fasta"):
        hit_name_matched = re.search(rf".*{ref_name}$", record.description)
        if hit_name_matched:
            extracted_seq = record.seq[ref_start - 1 : ref_end]
            if reversed_hit_frame:
                extracted_seq = extracted_seq.reverse_complement()
                # extracted_seq = extracted_seq[::-1]
            # uppercase the sequence
            # extracted_seq = extracted_seq.upper() # apparently case indicates quality of base call
            # Create a new sequence record
            hit_name = re.sub(r"\W+", "_", ref_name)
            new_seq_record = SeqRecord(
                extracted_seq,
                id=f"{row['locus']}_{record.id}_{hit_name}",
                name=record.name,
                description="",
            )
            # Read the output file if it exists, otherwise create a new list
            if original_file.exists():
                output_seqs = list(SeqIO.parse(original_file, "fasta"))
            else:
                output_seqs = []
            output_seqs = []
            # Append the new sequence record to the list and write it to the output file
            output_seqs.append(new_seq_record)
            with output_file.open("w") as output_handle:
                SeqIO.write(output_seqs, output_handle, "fasta")
    return output_file.parent


def extract_genome_sequence(
    row: dict[str, str],
    ref_folder: Path,
    query_folder: Path,
) -> None:
    """Extract the sequence from the reference file and write it to the output file

    Args:
        row (dict[str, str]): A dictionary containing the information about the sequence to be extracted
        ref_folder (Path): The path to the folder containing the reference files
        query_folder (Path): The path to the folder containing the query files
    """
    reference_file = ref_folder / f"{row['reference'].split('__')[1]}.fna"
    output_file = Path(
        f"{str(query_folder).replace('input/fasta','output/temp')}/genome_extracts.fasta"
    )
    output_file.parent.mkdir(parents=True, exist_ok=True)
    ref_start = int(row["ref_start"])
    ref_end = int(row["ref_end"])
    reversed_hit_frame = row["reversed_hit_frame"].lower() == "true"
    ref_name = row["hit_name"]
    locus = row["locus"]
    for record in SeqIO.parse(reference_file, "fasta"):
        hit_name_matched = re.search(rf".*{ref_name}$", record.description)
        if hit_name_matched:
            extracted_seq = record.seq[ref_start - 1 : ref_end]
            if reversed_hit_frame:
                extracted_seq = extracted_seq.reverse_complement()
            # uppercase the sequence
            # extracted_seq = extracted_seq.upper() # apparently case indicates quality of base call
            # Create a new sequence record
            hit_name = re.sub(r"\W+", "_", ref_name)
            new_seq_record = SeqRecord(
                extracted_seq,
                id=f"{locus}_{record.id}_{hit_name}",
                name=record.name,
                description="",
            )
            # Create a new list
            output_seqs = []
            # Append the new sequence record to the list and write it to the output file
            output_seqs.append(new_seq_record)
            with output_file.open("a") as output_handle:
                SeqIO.write(output_seqs, output_handle, "fasta")


def parallel_extract_loci(
    instruction_file: Path,
    ref_folder: Path,
    query_folder: Path,
) -> Path:
    """Perform the fasta operations in parallel

    Args:
        instruction_file (Path): The path to the instruction file
        ref_folder (Path): The path to the folder containing the reference files
        query_folder (Path): The path to the folder containing the query files
    """
    result = Path("")
    with open(instruction_file, "r") as csvfile:
        reader = csv.DictReader(csvfile)
        rows: list[dict[str, str]] = [row for row in reader]
    with ProcessPoolExecutor() as executor:
        futures = [
            executor.submit(extract_sequence, row, ref_folder, query_folder)
            for row in rows
        ]
        for future in tqdm(
            as_completed(futures), total=len(futures), desc="Processing"
        ):
            result = future.result()
    return result  # I just need one since they are all the same


def multiprocess_genome(
    instruction_file: Path,
    ref_folder: Path,
    query_folder: Path,
) -> None:
    """Perform the extract_genome_sequence in parallel on each F

    Args:
        instruction_file (Path): The path to the instruction file
        ref_folder (Path): The path to the folder containing the reference files
        query_folder (Path): The path to the folder containing the query files
    """
    with open(instruction_file, "r") as csvfile:
        reader = csv.DictReader(csvfile)
        rows: list[dict[str, str]] = [row for row in reader]
    with ProcessPoolExecutor() as executor:
        futures = [
            executor.submit(extract_genome_sequence, row, ref_folder, query_folder)
            for row in rows
        ]
        for future in tqdm(
            as_completed(futures), total=len(futures), desc="Processing"
        ):
            pass
